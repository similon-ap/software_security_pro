const logitor = require('logitor')('labo_hashing_encryption');
const CryptoJS = require('crypto-js');
const readlineSync = require('readline-sync');

function hashCode(str) {
    return str.split('').reduce((prevHash, currVal) =>
      (((prevHash << 5) - prevHash) + currVal.charCodeAt(0))|0, 0);
  }

let balance = 0;
let userDb = [
    {
        login: 'andie',
        balance: 1000000,
        pinCode: Math.abs(hashCode(logitor.getEmail())).toString().substr(0,4)
    },
    {
        login: 'frankie',
        balance: 50,
        pinCode: '1001'
    }
]

console.log('-----------------------');
console.log('WELCOME TO UNSAFE BANK ');
console.log('-----------------------');
console.log('');
while (true) {
    console.log(`Your current balance: ${balance}`);
    console.log('1. Print Database');
    console.log('2. Withdraw Money');
    console.log('9. Exit');
    let option = readlineSync.question('> ');

    if (option == 1) {
        console.log(`Login     | balance  | sha1`)
        console.log(`-----------------------------------------------------------------------`)

        userDb.forEach((user) => {
            console.log(`${user.login.padEnd(10,' ')}| $${user.balance.toString().padEnd(8,' ')}| ${CryptoJS.SHA1(user.pinCode).toString().padEnd(20,' ')}`);
        });
    } else if (option == 2) {
        let login = readlineSync.question('Login: ');
        let pin = readlineSync.question('PinCode: ');
        let withdrawn = false;
        for (let i=0;i<userDb.length;i++) {
            if (userDb[i].login === login && pin === userDb[i].pinCode) {
                console.log('Money withdrawn succesfully!');
                balance += userDb[i].balance;

                userDb[i].balance = 0;
                withdrawn = true;
            }
        }
        if (!withdrawn) {
            console.log('Unable to withdraw money!');
        }
    } else if (option == 9) {
        process.exit();
    }
}