# Encryptie Labo

Zoals altijd kunnen de oefeningen binnengehaald worden door een git pull te doen.

```
git pull
```

of de git repository te clonen als je deze nog niet hebt.

```
git clone https://bitbucket.org/similon-ap/software_security_pro.git
```

## Symmetrische encryptie met PGP
### Voorbereiding
- Kijk het volgende filmpje over PGP 

    <iframe src="https://ap.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=354f4aee-db18-4ea2-965e-ab7f01608063&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" width=420 height=236 style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>

- Lees het deel over symmetrische encryptie in het onderdeel GPG in de syllabus op gitbook.

    <a href="https://app.gitbook.com/@apwt/s/g-pro-software-security/hashing/encryptie-tools#symmetrische-encryptie">https://app.gitbook.com/@apwt/s/g-pro-software-security/hashing/encryptie-tools#symmetrische-encryptie</a>

### Stappenplan

1. In de directory labo_encryption/exercise1 staat een bestand ```book.txt```
2. Open een terminal venster zorg ervoor dat het bestand book.txt wordt geëncrypteerd met het paswoord: ```exercise1```. Je moet hier uiteraard gebruik maken van pgp.
3. Er staat ook een bestand ```bericht.txt.gpg``` met een geëncrypteerd bericht in. 
4. Zorg ervoor dat het bestand gedecrypteerd geraakt. Het paswoord waarmee het geëncrypteerd werd is ook ```exercise1```

## Asymmetrische encryptie met PGP
### Voorbereiding
- Lees het deel over asymmetrische encryptie in het onderdeel GPG in de syllabus op gitbook.

    https://app.gitbook.com/@apwt/s/g-pro-software-security/hashing/encryptie-tools#assymmetrische-encryptie

### Stappenplan

1. Ga naar de online pgp tool op 
https://smartninja-pgp.appspot.com/#
2. Genereer zelf een PGP keypair voor jezelf. Kies RSA als algoritme en een keysize van 1024bit. Zorg ervoor dat de sleutel nooit vervalt. Kies een passphrase en zorg ervoor dat je deze onthoudt.
3. Probeer een bericht te encrypteren met mijn publieke sleutel. Je mag hier zelf kiezen wat je stuurt. De publieke sleutel staat hier onder of je kan hem copy pasten vanuit het bestand exercise2/public_key.

    ```
    -----BEGIN PGP PUBLIC KEY BLOCK-----

    mI0EXm6FEAEEAModXdF/WFe3gEgSHQWhJYgNNm4RFD82Ed02ebTA/V8rmRdpPj1G
    WiK0Q2yeXU148Jk2o26OzjzUY5OtZSnsL/JmApvxQll9VJw9fmmnQ4QZiqZbJ6T8
    xUfE0vCDH+JoEJAh/Qx+fpqdvCPHMKQ6e8sqwV0iVkQVyqlA68ro0bNXABEBAAG0
    I0FuZGllIFNpbWlsb24gPGFuZGllLnNpbWlsb25AYXAuYmU+iLAEEwEKABoFAl5u
    hRACGy8DCwkHAxUKCAIeAQIXgAIZAQAKCRABLFt/RKGQbM7XBACZEPj3BLq314Yo
    Gq5ZvZy2XpjpWF7RlsjzxYuzZpiVeGBJcM1tU9pMcH+RhUgBkLEMqTYFhXYnDFXF
    va2JKtwcHGub63R1Tsi0Hldfpfn7eaoJyzde/O96nGbyBtlM6ovqDDnp+Agzi+y6
    PsvmaxIcxDGTUlFx/ki5sPwzCWVGXriNBF5uhRABBACudfMK0ROt/FGuVSTMsT2t
    9vlPv8jRz03b3MzFQxJ+4rhlmu/vEYyZbuPW7c+kF3Wt3VjwAamM8k064TmjXeE2
    CWjFRuzJDZwhcl4Bhj432C9q5NZa40vlan5nlU4zKz6aAiT4ScASOP4ytdzrp8F3
    iyPX0glKqYsEq5mSlh+WXwARAQABiQFDBBgBCgAPBQJeboUQBQkPCZwAAhsuAKgJ
    EAEsW39EoZBsnSAEGQEKAAYFAl5uhRAACgkQqjKNi2I2VEXzmAQAhN1VIOSNSSgf
    SS29EqpsFzc9KqBtpmwkCyn0/T6Ug9ZHL+Zbgzb1t56gj8U4/kJkqyDUV5MIYnKx
    BHSrKTmyh92BdSyyKcNHx5ew51njh+Z47ekRRA2y6/wy2aB2V7aT4apIEwifJy9G
    yg8EL6oHwqZP7o3ozikXpUJWmroUL5AQeAP+Kk9FWpRLSuucEETrX85ySqbyLlL5
    liL4NP1lxKwUxjoWK7JMfu7jbgqcpSTthvDXjih/He2TazjjaaAv8B2fQ2mpRUbH
    d52M+f+GYpKGoAWFui+67tfmdhh+U8b1VaZ3qnt1InNIcPQp3bd1L8ovXBhqSTSe
    GbuZR1/syzCCg224jQReboUQAQQA2eThBTTU1YXCOAXdHjD3WVG67JdRa8gexsmt
    g6DvmhztmEmT/heWgagxXEL/LEkhKmhKC9hRgTJJxJXwVkBhFYkvDxHB4Gx6cf60
    GRWQBDjnIbkdos8a3coyGWhhFElCZ/1C1Bo1oCke7UpRiYsho1Dc1mpcYNqDyjqJ
    wFbJopsAEQEAAYkBQwQYAQoADwUCXm6FEAUJDwmcAAIbLgCoCRABLFt/RKGQbJ0g
    BBkBCgAGBQJeboUQAAoJENaqHCN8sZrLJHwD/Roar4jUkM7fiSzjISirDbAIUGE3
    /Gs+gWvgeZUXd//ZVy5qReR1oewp6/aCWfxxEr+JmyFbP7ngSrOkATJagx2Y5WgL
    Ux5a/obUd0KAT9z11ZmLnFA1pPv0KpgK6qEu4fBnpw7dJtECi+F+U8cJMtOdA9vL
    mgjqNHpNnJISLZ5npQEEAMkp/e0yT+mUmojSYWMQ3gCtocPYLLpzVsa1ppmH757a
    qscD9GWpi4tlC6d4zrhDRvS9Pd65hmVvS1TrNiJPctxR0PcClT9TMhoN11v9SPP8
    g7GAXEeb89Dp9YTIqWhixFAJMjcWjZssqgfWI56Jd0i7l6QAIfJCawIGoBh7QFf/
    =Bhpk
    -----END PGP PUBLIC KEY BLOCK-----
    ```
4. Kopieer het geëncrypteerde bericht in het bestand encrypted.txt in labo_encrytion/exercise2.
5. Probeer met je eigen public key zelf een bericht naar jezelf te encrypteren en daarna vervolgens te decrypteren. 
6. Als extra kan je eens een bericht naar jezelf proberen te sturen met je eigen publieke sleutel. Of je probeert eens samen met iemand anders geheime berichten naar elkaar te sturen.

## Symmetrische encryptie met cryptoJS
### Voorbereiding
- Kijk het volgende filmpje over Symmetrische Encryptie met CryptoJS
    
    <iframe src="https://ap.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=94a08d30-6aec-4deb-a70c-ab7f016c17ea&autoplay=false&offerviewer=true&showtitle=true&showbrand=false&start=0&interactivity=all" width=420 height=236 style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>
    
- Lees het deel over Encryptie Algoritmes onder het onderdeel cryptoJS op

    <a href="https://app.gitbook.com/@apwt/s/g-pro-software-security/hashing/crypto.js#encryptie-algoritmes">https://app.gitbook.com/@apwt/s/g-pro-software-security/hashing/crypto.js#encryptie-algoritmes</a>

### Stappenplan

- Ga naar labo_encryption/exercise3 in visual studio code.
- Voer ```npm install``` uit in de terminal zodat alle dependencies geinstalleerd worden.
- Open het bestand encryption.js en implementeer (maak) de encrypt en decrypt methode zodat de testen slagen. 

    Je kan de testen lopen door

    ```
    npm test
    ``` 

    uit te voeren.

- Open het index.js bestand en gebruik de encrypt methode om een tekstje te encrypteren en zorg ervoor dat je daarna het bericht terug decrypteerd gebruikmakend van een paswoord.