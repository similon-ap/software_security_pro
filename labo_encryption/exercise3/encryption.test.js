const encryption = require('./encryption');

describe('testing the encryption module', () => {
    test('test the encrypt function does not return the message unencrypted', () => {
        expect(encryption.encrypt('this is a message','password')).not.toBe('this is a message');
    })

    test('test the decrypt function', () => {
        expect(encryption.decrypt('U2FsdGVkX1+foh7TmaJO2n/KVs1LknvSxxTWPK/ZhFF2fhGTn1yLis/gW+hbr/Y2', 'password')).toBe('this is a message');
    });
});