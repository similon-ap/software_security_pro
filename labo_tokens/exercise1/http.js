const express = require('express');
const https = require('https');
const http = require('http');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const logitor = require('logitor')('labo_tokens');
const phonetic = require('phonetic');
const assert = require('assert');

const app = express();
app.use(express.static('public'))
app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }))

const jwtKey = 'my_secret_key';
const jwtExpirySeconds = 3600;

// Login endpoint
app.get('/login', (req, res) => {
    let username = req.query.username;
    let password = req.query.password;
    if (username === logitor.getEmail() && password === 'admin123') {
        const token = jwt.sign({ username }, jwtKey, {
            algorithm: 'HS256',
            expiresIn: jwtExpirySeconds
        });
        res.send({ token });
    } else {
        res.statusCode = 401;
        res.send({ error: 'Wrong username and password' });
    }
});

app.get('/message', (req, res) => {
    try {
        let authHeader = req.get('authorization');
        let jwtToken = '';
        try {
            jwtToken = authHeader.split(' ')[1];
        } catch (e) {
        }
        jwt.verify(jwtToken, jwtKey)
        res.send({message: 'The secret word is: ' + phonetic.generate({ seed: logitor.getEmail() })});
    } catch (e) {
        console.log(e);
        res.status(401);
        res.send(e);
    }
});

app.get('/task', (req, res) => {
    res.render('task', {email: logitor.getEmail(), errors: {}, answers: {}, hasErrors: undefined});
});

const sanitize = (s) => {
    return s.replace( /[\r\n]+/gm, "" ).trim();
}

app.post('/task', (req, res) => {
    let json = Object.assign({},req.body);
    let errors = {};
    errors.status_code_login = (json.status_code_login.includes('401'));
    errors.jwt_token_1 = false;
    errors.jwt_decoded_header = false;
    errors.jwt_decoded_payload = false;
    errors.token_expiry = false;
    try {
        let decoded = jwt.decode(sanitize(json.jwt_token_1), {complete: true});
        errors.jwt_token_1 = (decoded.payload.username === logitor.getEmail());
        errors.jwt_decoded_header = deepEqual(decoded.header, JSON.parse(sanitize(json.jwt_decoded_header)));
        let decodedPayload = JSON.parse(sanitize(json.jwt_decoded_payload));
        errors.jwt_decoded_payload = (decoded.payload.username === decodedPayload.username) && (decoded.payload.iat === decodedPayload.iat);
        errors.token_expiry = new Date(decodedPayload.exp*1000).getTime() == new Date(json.token_expiry).getTime();
    } catch (e) {
        console.log(e);
    }
    try {
        errors.response_no_token = sanitize(json.response_no_token).includes('jwt must be provided');
    } catch (e) {
        errors.response_no_token = false;
    }
    
    try {
        errors.message_call_response = sanitize(json.message_call_response).includes(phonetic.generate({ seed: logitor.getEmail() }));
    } catch (e) {
        errors.message_call_response = false;
    }
    try {
        errors.message_call_changed_key = sanitize(json.message_call_changed_key).includes('invalid signature');
    } catch (e) {
        errors.message_call_changed_key = false;
    }

    try {
        errors.expired_token_response = sanitize(json.expired_token_response).includes('jwt expired');
    } catch (e) {
        errors.expired_token_response = false;
    }
    
    try {
        errors.jwt_decoded_with_password_header = ((JSON.parse(sanitize(json.jwt_decoded_with_password_header))).alg === 'HS256')
    } catch (e) {
        errors.jwt_decoded_with_password_header = false;
    }
    try {
        errors.jwt_decoded_with_password_payload = (JSON.parse(sanitize(json.jwt_decoded_with_password_payload)).password == 'admin123');
    } catch (e) {
        errors.jwt_decoded_with_password_payload = false;
    }

    
    let allCorrect = Object.keys(errors).reduce((prev, curr) => {
        return prev && errors[curr] === true;
    }, true);

    if (req.body.check) {
        res.render('task', {email: logitor.getEmail(), errors: errors, answers: req.body, hasErrors: !allCorrect});
    }
    if (req.body.submit) {
        res.setHeader('Content-disposition', 'attachment; filename=' + logitor.getEmail() + '.txt');
        res.setHeader('Content-type', 'text/plain');
        res.send({email: logitor.getEmail(), errors: errors, answers: req.body, hasErrors: !allCorrect});
    }

});

const deepEqual = (obj1,obj2) => {
    try {
        assert.deepEqual(obj1,obj2)
        return true;
    } catch (e) {
        return false;
    }
}

http.createServer(app).listen(3000, () => {
    console.log('HTTP server started on http://localhost:3000');
});