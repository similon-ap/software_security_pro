<!-- 
DISCLAIMER:

!!!!

GEBRUIK NIET DIT BESTAND OM HET LABO TE MAKEN MAAR GEBRUIK DE LINK OP DIGITAP!

!!!!
-->

<style>
.holder_default {
    width:500px; 
    height:150px; 
    border: 3px dashed #ccc;
}

.hover { 
    width:400px; 
    height:150px; 
    border: 3px dashed #0c0 !important; 
}

.hidden {
    visibility: hidden;
}

.visible {
    visibility: visible;
}
</style>

<script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

<form action="/task" method="post">

# Tokens

Zoals altijd kunnen de oefeningen binnengehaald worden door een git pull te doen.

```
git pull
```

of de git repository te clonen als je deze nog niet hebt.

```
git clone https://bitbucket.org/similon-ap/software_security_pro.git
```

### Wat ga je leren in dit labo?
- Leren werken met Postman.
- De werking van JWT tokens begrijpen en gebruiken.
- Concepten als secret key en expiration van een token begrijpen.

### Stappenplan

1. Open de terminal in ```labo_tokens/exercise1``` via visual studio code en installeer alle npm dependencies met

    ```
    npm install
    ```

2. We hebben voor deze oefening net als vorig labo een webserver in node js geschreven. Je kan deze opstarten met

    ```
    node http.js naam.achternaam@student.ap.be
    ```

    Deze keer is er geen https server aanwezig dus certificaten zijn hier niet nodig. 

3. Ga vervolgens naar <a href="http://localhost:3000/task">http://localhost:3000/task</a> en daar zal je de rest van het gepersonaliseerde labo te zien krijgen.

</form>