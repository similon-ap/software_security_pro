<!-- 
DISCLAIMER:

!!!!

GEBRUIK NIET DIT BESTAND OM HET LABO TE MAKEN MAAR GEBRUIK DE LINK OP DIGITAP!

!!!!
-->

<style>
.holder_default {
    width:500px; 
    height:150px; 
    border: 3px dashed #ccc;
}

.hover { 
    width:400px; 
    height:150px; 
    border: 3px dashed #0c0 !important; 
}

.hidden {
    visibility: hidden;
}

.visible {
    visibility: visible;
}
</style>

<script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

<form action="/task" method="post">

# Tokens

Zoals altijd kunnen de oefeningen binnengehaald worden door een git pull te doen.

```
git pull
```

of de git repository te clonen als je deze nog niet hebt.

```
git clone https://bitbucket.org/similon-ap/software_security_pro.git
```

### Wat ga je leren in dit labo?
- Leren werken met Postman.
- De werking van JWT tokens begrijpen en gebruiken.
- Concepten als secret key en expiration van een token begrijpen.

### Stappenplan

1. Open de terminal in ```labo_tokens/exercise1``` via visual studio code en installeer alle npm dependencies met

    ```
    npm install
    ```

2. We hebben voor deze oefening net als vorig labo een webserver in node js geschreven. Je kan deze opstarten met

    ```
    node http.js naam.achternaam@student.ap.be
    ```

    Deze keer is er geen https server aanwezig dus certificaten zijn hier niet nodig. 

3. Als je naar ```http://localhost:3000/task ``` surft kom je op deze pagina terecht.

3. Installeer Postman door naar het adres https://www.postman.com te gaan en de installatie procedure te doorlopen.

4. Maak in postman een nieuwe GET request aan met een willekeurig gekozen username en passwoord:

    <img src="newrequest2.png" width="640">

   ```http://localhost:3000/login?username=naam.achternaam@student.ap.be&password=password```

5. Zoek de status code die de server terug geeft in postman. **Vul deze hier in:**

    <input name="status_code_login" type="number">

    (geef enkel de token in, niet de volledige json response)

6. Maak in postman een nieuwe GET request aan in Postman maar doe deze nu naar

    ```http://localhost:3000/login?username=naam.achternaam@student.ap.be&password=admin123```

    Je krijgt vervolgens een token terug. 
    
    **Geef deze hier in:**

    <textarea name="jwt_token_1" style="width: 100%">
    </textarea>

7. Je kan op de website ```https://www.jsonwebtoken.io/``` het token decoderen en bezien wat de inhoud er van is. Plak je JWT token in het JWT string veld.

    **Kopieer hier het header veld:**
    <textarea name="jwt_decoded_header" style="width: 100%; height: 100px">
    </textarea>

    **Kopieer hier het payload veld:**
    <textarea name="jwt_decoded_payload" style="width: 100%; height: 100px">
    </textarea>

8. Naast informatie die vrij te kiezen is door de server bevat de JWT token twee belangrijke timestamps:

    - **iat**: De unix timestamp wanneer de JWT token aangemaakt werd
    - **exp**: De unix timestamp wanneer de JWT token zal vervallen

    Kijk na wanneer jou token vervalt en zet het om naar het ISO 8601 datum formaat.
    
    Gebruik https://www.unixtimestamp.com om dit te doen. 

    **Vul deze hieronder in:**
    <textarea name="token_expiry" style="width: 100%;">
    </textarea>

9. Maak in postman een nieuwe GET request naar 

    ```http://localhost:3000/message```

    Wat is de response van deze request?

    <textarea name="response_no_token" style="width: 100%; height: 100px">
    </textarea>

10. Deze message API call is beveiligd en verwacht een JWT token. Je kan deze JWT token ingeven in 

    ```Authorization -> Type -> Bearer```

    <img src="bearerpostman.png" width="640">

    Daar kan je de bovenstaande JWT token ingeven en de request opnieuw doen. 

    **Geef hier de response van deze request:**

    <textarea name="message_call_response" style="width: 100%; height: 100px">
    </textarea>

11. Open het bestand ```http.js``` en pas de variabele ```jwtKey``` aan naar een andere secret key (vrij te kiezen) en herstart dan de http server.

12. Probeer opnieuw de GET request te doen naar ```http://localhost:3000/message``` met dezelfde token. Je zal zien dat dit niet werkt. Dit is omdat het token nu niet meer gesigned is met dezelfde secret key.

13. Log nu terug in via de login endpoint en doe opnieuw de GET request naar het ```/message``` endpoint met de nieuwe JWT token.

14. Ga terug naar ```https://www.jsonwebtoken.io/``` en kopieer en plak de token in het JWT string veld. Vul je secret key in het Signing Key veld en dan zal je zien dat je ```verified``` ziet verschijnen. 

15. Open het bestand ```http.js``` en pas de variabele ```jwtExpirySeconds``` aan naar 120 seconden en herstart dan de http server.

16. Log nu terug in via de login endpoint en gebruik deze token om terug de 
    
    ```http://localhost:3000/message``` 
    
    request te doen (met de nieuwe token).

17. Wacht nu 2 minuten en doe terug een request naar de message endpoint met dezelfde token. Dit zou niet mogen werken want de token is nu vervallen.

    **Geef hier onder de response die je terug krijgt:**
    <textarea name="expired_token_response" style="width: 100%; height: 100px">
    </textarea>

18. Om te demonstreren dat het niet de bedoeling is om gevoelige data zoals een paswoord in een JWT token te steken gaan we nog een aanpassing doen aan het ```http.js``` bestand:

    pas het volgende stuk code aan:

    ```javascript
    const token = jwt.sign({ username }, jwtKey, {
            algorithm: 'HS256',
            expiresIn: jwtExpirySeconds
        });
    ```

    naar

    ```javascript
    const token = jwt.sign({ username, password }, jwtKey, {
            algorithm: 'HS256',
            expiresIn: jwtExpirySeconds
    });
    ```

    Dit zorgt ervoor dat naast je username ook je passwoord in het jwt token zal staan. 

19. Herstart de server en log terug in via het login endpoint.

20. Gebruik terug ```https://www.jsonwebtoken.io/``` om de nieuwe token te decoderen. Je zal nu zien dat het paswoord 'admin123' er gewoon leesbaar in staat. Zet dus nooit gevoelige data in de JWT token zelf want deze is publiek leesbaar.

    **Kopieer hier het header veld:**
    <textarea name="jwt_decoded_with_password_header" style="width: 100%;height: 100px">
    </textarea>

    **Kopieer hier het payload veld:**
    <textarea name="jwt_decoded_with_password_payload" style="width: 100%;height: 100px">
    </textarea>

21. Je kan je oplossingen nakijken of deze correct zijn. Dit is enkel een indicatie en geen eindscore.

    <input type="submit" value="Kijk oplossingen na" name="check">

    Fouten worden aangegeven met een rood kader. 

22. Vervolgens kan je het oplossingsbestand downloaden en moet je dit inzenden via digitap.

    <input type="submit" value="Kijk oplossingen na" name="submit">
</form>