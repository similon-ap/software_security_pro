const calculate = (operator, a,b) => {
    if (operator === '+') {
        return a+b;
    } else if (operator === '-') {
        return a-b;
    } else if (operator === '*') {
        return a*b;
    } else if (operator === '/') {
        if (b === 0) {
            throw 'cannot divide by zero';
        }
        return a/b;
    } else {
        throw 'operator unknown';
    }
}

exports.calculate = calculate;