const findNumberInArray = (array, number) => {
    if (isNaN(number)) {
        throw `${number} is geen getal`;
    }
    for (let i=0;i<array.length;i++) {
        if (array[i] === number) {
            return i;
        }
    }
    return -1;
}

exports.findNumberInArray = findNumberInArray;