<!-- 
DISCLAIMER:

!!!!

GEBRUIK NIET DIT BESTAND OM HET LABO TE MAKEN MAAR GEBRUIK DE LINK OP DIGITAP!

!!!!
-->

<style>
.holder_default {
    width:500px; 
    height:150px; 
    border: 3px dashed #ccc;
}

.hover { 
    width:400px; 
    height:150px; 
    border: 3px dashed #0c0 !important; 
}

.hidden {
    visibility: hidden;
}

.visible {
    visibility: visible;
}
</style>

<script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

<div id="alles">

# Test Driven Development

Zoals altijd kunnen de oefeningen binnengehaald worden door een git pull te doen.

```
git pull
```

of de git repository te clonen als je deze nog niet hebt.

```
git clone https://bitbucket.org/similon-ap/software_security_pro.git
```

### Wat ga je leren in dit labo?
- We herhalen de reeds geleerde concepten over testing

### Stappenplan

1. Open de terminal in ```labo_tdd_3/exercise1``` via visual studio code en installeer alle npm dependencies met

    ```
    npm install
    ```

2. Om te zien of alles werkt hebben we 1 eenvoudige test toegevoegd die gewoon nakijkt of de waarde true gelijk is aan true. Die test gaat uiteraard altijd slagen. Voer het commando ```npm test``` uit om de test uit te voeren.

3. In het bestand ```array.js``` vind je een door ons geschreven functie ```findNumberInArray```. Deze functie neemt een array en een integer als argument. Als return waarde zal hij de index teruggeven van het getal in de array. 

4. Je gaat in dit labo een aantal testen schrijven. Deze moeten in het bestand ```exercise1.test.js``` komen. 

5. Schrijf een test die test dat als je de array [1,2,3] meegeeft en het getal 2, dat de functie de index 1 teruggeeft. Ter herhaling een test bestaat altijd uit de volgende vorm:

    ```javascript
    test('beschrijving van de test', () => {
        expect(functieDieJeWilTesten(parameters)).toBe(verwachteWaarde);
    })
    ```

6. Als je het commando ```npm test``` uitvoert in de terminal zou je moeten zien dat er 2 testen geslaagd zijn.

7. De functie ```findNumberInArray``` geeft het getal -1 terug als het getal niet kan gevonden worden. Schrijf hier een test voor.

8. Als je in plaats van een getal een letter als 2de argument meegeeft dan zal de functie een error gooien. Als je bijvoorbeeld: de letter a meegeeft dan krijg je als error boodschap ```'a is geen getal'``` terug. 

    Schrijf hier een test voor. 

9. Nu we terug hebben herhaald hoe een test werkt gaan we nu eens andersom werken. In test driven development schrijven je altijd eerst zelf een test en daarna pas maak je de functie aan. 

10. Voeg de volgende testen toe aan het ```exercise1.test.js``` bestand:

    ```javascript
        test('dat de functie concat twee strings aan elkaar voegt', () => {
            expect(concat('hello', 'world')).toBe('helloworld');
        });

        test('dat de functie concat twee getallen aan elkaar voegt en niet optelt', () => {
            expect(concat(4, 3)).toBe('43');
        });
    ```

11. Ga naar het bestand ```strings.js``` en pas de functie concat zo aan dat deze bovenstaande testen slagen.

    **Tip:** Zoek op hoe je getallen kan omzetten naar strings in javascript. Google is je vriend!

12. De laatste functie die we gaan testen is de ```calculate``` functie. Deze bevind zich in het ```numbers.js``` bestand.  Het is de bedoeling om de functie zo grondig mogelijk te testen. Daarvoor gaan we gebruik maken van de coverage functionaliteit van jest om te zien hoe goed deze functie getest is.

13. Ga naar de terminal en voer het volgende commando uit

    ```
    npm run coverage
    ```

    Je krijgt een overzicht van de coverage van de testen. Om in detail te kijken welke lijnen code al afgetest zijn, open je het bestand ```coverage/lcov-report/index.html``` in je browser.

    <img src="coverage-report.png" width="400">

    De rode lijnen geven aan welke lijnen code nog niet afgetest zijn.

14. Schrijf testen voor de calculate functie zodat alle lijnen getest zijn. Het is dus de bedoeling om hier 100% statement coverage te krijgen (% Stmts)

    Elke keer dat je een test bijgeschreven heb kan je de ```npm run coverage``` commando terug uitvoeren om te zien hoeveel % je hebt 'geraakt'.

15. Maak een zip file van alle javascript bestanden en stuur deze door via digitap. De ```node_modules``` moeten hier niet toegevoegd worden. Ze vergroten alleen maar je zip file. 