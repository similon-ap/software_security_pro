// fun: een functie die kan doorgegeven worden als argument. Deze functie zal toegepast worden op a en b.
exports.call = (fun, a, b) => {
    fun(a,b);
}

// fun: een functie die kan doorgegeven worden als argument. Deze functie zal toegepast worden op a en b.
// times: hoeveel keer de functie moet aangeroepen worden
exports.callMultiple = (fun, times, a, b) => {
    for (let i = 0; i<times;i++) {
        fun(a,b);
    }
}