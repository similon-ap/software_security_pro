---
marp: true
---
![bg left:40% 80%](oop.png)

# **Software Security: TDD2**
Nog meer testen (+ coverage)

---

# Hoe starten?

**Hoe nieuwe oefeningen binnenhalen via git?**
- git pull 
(of "Ctrl-shift-P" en Git pull intypen)

**Ik was er vorige keer niet?**
- Hoe git repository clonen?

    ```
    git clone https://similon-ap@bitbucket.org/similon-ap/software_security_pro.git
    ```
    (of Ctrl-shift-P en dan git clone intypen)

---

# Interessante extension voor jest

- Installeer de extension 'jest' van Orta
- **Ctrl-shift-p** : Jest start-runner

Testen worden automatisch gelopen en je krijgt nu ook code completion.

---

# Coverage

- npm run coverage (vergeet **npm install** niet!)
- Maakt een coverage directory aan in je project.
- open **coverage/lcov-report/index.html**
- **Ctrl-shift-p** : Jest toggle coverage overlay (als je jest plugin hebt)


