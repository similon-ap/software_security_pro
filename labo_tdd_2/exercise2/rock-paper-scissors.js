const VALID = ['rock','paper','scissors'];
/**
 * 
 * play geeft het resultaat van rock paper scissors terug. 
 * 
 * choice1: de keuze van speler 1 (rock,paper of scissors)
 * choice2: de keuze van speler 2 (rock,paper of scissors)
 * 
 */
exports.play = (choice1, choice2) => {
    if (VALID.includes(choice1) && VALID.includes(choice1)) {
        if (choice1 == choice2) {
            return 'The result is a tie!';
        } else if (choice1 == 'rock') {
            if (choice2 == 'scissors') {
                return 'rock wins';
            } else {
                return 'paper wins';
            }
        } else if (choice1 == 'paper') {
            if (choice2 == 'rock') {
                return 'paper wins';
            } else {
                return 'scissors wins';
            }
        } else if (choice1 == 'scissors') {
            if (choice2 == 'rock') {
                return 'rock wins';
            } else {
                return 'scissors wins';
            }
        }
    } else {
        return undefined;
    }
}