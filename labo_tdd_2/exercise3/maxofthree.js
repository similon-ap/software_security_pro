/**
 * 
 * maxofthree geeft het grootste getal terug van 3 meegegeven getallen.
 * 
 * x moet een getal zijn.
 * y moet een getal zijn.
 * z moet een getal zijn.
 * 
 * Error afhandeling:
 *    'a' is not a number.
 *    'a', 'b' are not numbers.
 *    ...
 */

const checkInput = (x,y,z) => {
    let arr = [];
    if (isNaN(x)) {
        arr.push(x.toString());
    }
    if (isNaN(y)) {
        arr.push(y.toString());
    }
    if (isNaN(z)) {
        arr.push(z.toString());
    }
    if (arr.length == 1) {
        throw new Error(arr[0] + ' is not number.')
    }
    if (arr.length > 1) {
        throw new Error(arr.join(',') + ' are not numbers.')
    }
}

exports.maxofthree = (x,y,z) => {
    checkInput(x,y,z);
    max_val = 0;
    if (x > y) {
        max_val = x;
    } else {
        max_val = y;
    }
    if (z > max_val) {
        max_val = z;
    }
    return max_val;
}