const app = require('./app');
const math = require('./math');

math.add = jest.fn();
math.subtract = jest.fn();

test('calls math.add', () => {
  app.doAdd(1, 2);
  // stap 1: kijk na of de mocked functie is aangeroepen met 1 en 2
});

// stap 2: schrijf de testen voor doSubtract, doMultiply and doDivide