exports.add = (a, b) => { 
    return a + b;
};

exports.subtract = (a, b) => {
    return b - a;
}

exports.multiply = (a, b) => {
    return a * b;
}

exports.divide   = (a, b) => {
    return b / a;
}