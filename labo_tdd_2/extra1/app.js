const math = require('./math');

exports.doAdd = (a, b) => {
    return math.add(a, b);
}

exports.doSubtract = (a, b) => {
    return math.subtract(a, b);
}

exports.doMultiply = (a, b) => {
    return math.multiply(a, b);
}

exports.doDivide = (a, b) => {
    return math.divide(a, b);
}