/**
 * 
 * daysTillXmas geeft het aantal dagen terug tot kerstdag
 * 
 * day: dag van de maand
 * month: maand
 *
 */
const DAY = 1000 * 60 * 60 * 24;

const checkInput = (day, month) => {
    let d = new Date(new Date().getFullYear(), month-1, day);
    if (d.getDate() != day) {
        throw new Error('Invalid date!');
    }
}

exports.daysTillXmas = (day, month) => {
    checkInput(day,month);
    let today = new Date(new Date().getFullYear(), month-1, day);
    let cmas = new Date(new Date().getFullYear(), 11, 25);
    if (month-1 == 11 && day > 25) {
        cmas.setFullYear(cmas.getFullYear() + 1);
    }
    return Math.ceil((cmas.getTime()-today.getTime())/(DAY));
}