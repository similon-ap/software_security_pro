const math = require('./math');

describe('Testing the math module.', () => {
    test('5 + 5 = 10', () => {
        expect(math.sum(5,5)).toEqual(10);
    });

    test('4 + 1 = 5', () => {
        expect(math.sum(4,1)).toEqual(5);
    });

    test('a + b = Error', () => {
        expect(() => { math.sum('a','b')} ).toThrowError(new Error('This is not a number'));
    })
});
