const snake = require('./snake');

describe('Snake test', () => {
    test('Snake movement UP', () => {
        expect(snake.handleKeyPress('', {name: 'up'})).toBe('N');
    });

    test('Snake movement DOWN', () => {
        expect(snake.handleKeyPress('', {name: 'down'})).toBe('U');
    });

    test('Snake movement LEFT', () => {
        expect(snake.handleKeyPress('', {name: 'left'})).toBe('W');
    });

    test('Snake movement RIGHT', () => {
        expect(snake.handleKeyPress('', {name: 'right'})).toBe('E');
    });

    test('isFood is working properly', () => {
        expect(snake.isFood([['$']],0,0)).toBeTruthy();
    })

    test('render world properly', () => {
        let snakeArray = [ [ 4, 4 ], [ 3, 4 ] ];
        let worldArray = [
            [
              '+', '-', '-', '-',
              '-', '-', '-', '-',
              '-', '+'
            ],
            [
              '|', ' ', ' ', ' ',
              ' ', ' ', ' ', ' ',
              ' ', '|'
            ],
            [
              '|', ' ', ' ', ' ',
              ' ', ' ', ' ', ' ',
              ' ', '|'
            ],
            [
              '|', ' ', ' ', ' ',
              ' ', ' ', ' ', ' ',
              ' ', '|'
            ],
            [
              '|', ' ', ' ', ' ',
              ' ', ' ', ' ', ' ',
              ' ', '|'
            ],
            [
              '|', ' ', ' ', ' ',
              ' ', ' ', ' ', ' ',
              ' ', '|'
            ],
            [
              '|', ' ', ' ', ' ',
              ' ', ' ', ' ', ' ',
              ' ', '|'
            ],
            [
              '|', ' ', ' ', ' ',
              ' ', ' ', ' ', ' ',
              ' ', '|'
            ],
            [
              '|', ' ', ' ', ' ',
              ' ', ' ', ' ', ' ',
              ' ', '|'
            ],
            [
              '+', '-', '-', '-',
              '-', '-', '-', '-',
              '-', '+'
            ]
          ];

          let expected = '+--------+\n' +
                         '|        |\n' +
                         '|        |\n' +
                         '|   o    |\n' +
                         '|   O    |\n' +
                         '|        |\n' +
                         '|        |\n' +
                         '|        |\n' +
                         '|        |\n' +
                         '+--------+\n';

          expect(snake.world2string(worldArray, snakeArray)).toBe(expected);

    })


});
