const hello = require('./hello');

describe('Hello test module', () => {
    test('test hello andie', () => {
        expect(hello.hello('ANDIE')).toBe('Hello ANDIE!');
    });
});