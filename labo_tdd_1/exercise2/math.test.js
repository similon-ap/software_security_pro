const math = require('./math');

let numbers = [];
let max = 0;

const initEntireTest = () => {
    max = 10;
}

const initNumbers = () => {
    for (let i=1;i<=max;i++) {
        numbers.push(i);
    }
}

const clearNumbers = () => {
    numbers = [];
}

// 1: Zorg ervoor dat de initEntireTest() functie eenmaal 
//    wordt uitgevoerd voor het lopen van alle testen.
// 2: Zorg ervoor dat de initNumbers() functie elke keer
//    wordt uitgevoerd voor een lopen van een test.
// 2: Zorg ervoor dat de clearNumbers() functie elke keer
//    na het lopen van een test wordt uitgevoerd.
describe('Testing the math module.', () => {

    test('Add numbers from 1 to 10', () => {
        let sum = 0;
        for (let i=0;i<numbers.length;i++) {
            sum = math.add(sum, numbers[i]);
        }
        expect(sum).toEqual(55);
    });

    test('Multiply numbers from 1 to 10', () => {
        let mult = 1;
        console.log(numbers.length);
        for (let i=0;i<numbers.length;i++) {
            mult = math.mult(mult, numbers[i]);
        }
        expect(mult).toEqual(3628800);
    });

});
