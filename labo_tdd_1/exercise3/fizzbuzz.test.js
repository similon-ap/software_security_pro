const fb = require('./fizzbuzz');

/** 
 * 
 * Is een spel voor kinderen dat ze helpt leren delen door getallen. Spelers moeten om de beurt stapsgewijs een getal roepen 
 * (1, 2, 3, 4…), als het getal deelbaar is door drie zeggen ze het woord “Fizz”, is het getal deelbaar door vijf zeggen ze 
 * het woord “Buzz”. Zoals hieronder:
 * 
 * 1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, Fizz Buzz, 16, 17, Fizz, 19, Buzz, Fizz, 22, 23, Fizz, Buzz, 
 * 26, Fizz, 28, 29, Fizz Buzz, 31, 32, Fizz, 34, Buzz, Fizz, …
 * 
 * Ook wel gebruikt als
 * ...drank spel. Wellicht dat de meesten van jullie het spel op deze manier hebben leren kennen. 
 * Iedereen die meedoet en niet in staat is om op de juiste momenten “Fizz”, “Buzz” of “Fizz Buzz” te roepen moet drinken.
 * 
 * ...sollicitatievraag. Deze vraag wordt vaak gebruikt om het kaf van het koren te te scheiden.
 * 
 * Opdracht: 
 * Schrijf een aantal testen die alle verschillende mogelijkheden aftoetst zodat elke lijn code getest is.
**/
describe('Testing the fizzbuzz module.', () => {

});
