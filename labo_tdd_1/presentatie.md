---
marp: true
---
![bg left:40% 80%](oop.png)

# **Software Security**

**Test Driven Development**

Andie Similon
Sven Charleer

---

# **Voorbereid?**
- Visual Studio Code geinstalleerd?
- Node geïnstalleerd?
- Git geïnstalleerd?
- **(Windows only)** Git-bash als default terminal?

---
# **Oefeningen**
- Open een terminal (bash/cmd/powershell) en voer uit:
```bash
git clone https://similon-ap@bitbucket.org/similon-ap/software_security_pro.git
```

---
# **Werkt alles?**
- Open Visual Studio Code
- File -> Open Folder -> Open de folder met software_security_pro
- Terminal -> New Terminal
```
cd exercise0
npm install
npm test
```
- PASS?