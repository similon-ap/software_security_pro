const CryptoJS = require('crypto-js');
const utils = require('./utils');

const salt = CryptoJS.lib.WordArray.random().toString();
const iterations = 1;

const hash = (password) => {
    return password;
}

const encrypt = (text, password) => {
    return text;
}

const decrypt = (encryptedText, password) => {
    return encryptedText;
}

exports.hash = utils.time(hash);
exports.salt = salt;
exports.iterations = iterations;
exports.encrypt = encrypt;
exports.decrypt = decrypt;