const openpgp = require('openpgp'); // use as CommonJS, AMD, ES6 module or via window.openpgp
 
(async () => {
 
    // put keys in backtick (``) to avoid errors caused by spaces or tabs
    const publicKeyArmored = `-----BEGIN PGP PUBLIC KEY BLOCK-----
    Version: Keybase OpenPGP v2.0.9
    Comment: https://keybase.io/crypto
    
    xo0EXrhIDgEEAMKd3Ykwzzyn6TAPJylJYbmc8iljqjcHx9XW/Ztj6DR2rKd+IHl+
    rlOTeNNPxVQ57ZGHRzEoB5CmLkBJx5qyXKrEK8Qo3W0BVyYTcIrnGSKq2GmvpdHZ
    Y9Fykl0FsTgSMpvXptz73xoMQSIwLpGkXOMJCd/gC2/L0xLrmWxAjO6LABEBAAHN
    K0FuZGllIFNpbWlsb24gKGQpIDxhbmRpZS5zaW1pbG9uQGdtYWlsLmNvbT7CsAQT
    AQoAGgUCXrhIDgIbLwMLCQcDFQoIAh4BAheAAhkBAAoJEBVQcPb4rmU5mh0EAJzF
    d2ov5MJ4Ivo+UCCAQD8Jg2MhVrAKTZTA7mWmNTCG6Cr7DG2v9463viOQWw4syfxt
    nbeWLAvQJZrNj6Iypa2+4Q+ofUOpd543L1Yx0irvT8iifWbx0IzbpSLvKFr9GPsT
    hYCpSRMEAKuBH9/TFoOO+xBT/SPjInCfT24o7+Gszo0EXrhIDgEEAONPSVXbDL8H
    nEAmjsDfPaEvmSeoSH9Gyl+VpNMVdFZ9EthSELKBc27rQ3oKqfHTHgAazaE3wX7Z
    Oc3WPnALipE5uuUSJskF3eA+h5H8WWV4DbD1uYNANYMTWroNxMQ8HTqnQt+ckeFn
    8/sWLly5tTBFM0W0ITANgNVZTScjiJnjABEBAAHCwIMEGAEKAA8FAl64SA4FCQ8J
    nAACGy4AqAkQFVBw9viuZTmdIAQZAQoABgUCXrhIDgAKCRB19rB1WVBbOhUDA/9j
    0/RPUOMNc9I/LQgyuaGSRbmn2miLYJmDLkOxIJdEjg56i9SNc057f7djN+F2D+8/
    k/K/0N+3CdjWClpBoMuauhpHdtPCk220TByZ0QpTZXEFM7WMKt3WMuARqe7gKEWr
    k+5zZ3cUGAwwgJOIMlyx33n83KcZSPQbvQ66dK3YE6qKA/4/3pf1qr/D1KpE3Z7f
    /0Ky3DZaedLcVtBi1beiKdBLL3spaDHyu/7c+ErRrf+6XFOwwHqLN80QgtexIOb9
    tuJgr/V8suAtR7t+HXuh6VIlUoOPV9pkZVLV8U6BUgCcEva9x+u9qIq8EXeX8vgS
    jwGPKEnzSN1hJLFp28avBJmPd86NBF64SA4BBAC7ScjuxcwbBO1M6H6fYqCQC4gD
    CotMO13HB6WykGUeXCQZNJVs9/0Z6NEnkK8GOsIKAVO11LcNTAiqznkSmik1yfpp
    UmyHtAc6SVIUZ/xUTgSpF9pWC8kjFtWlXOseiC5rgjXPy5eOlKULtIquP4bkT34h
    /fbR7fL2W0cdDXKfLQARAQABwsCDBBgBCgAPBQJeuEgOBQkPCZwAAhsuAKgJEBVQ
    cPb4rmU5nSAEGQEKAAYFAl64SA4ACgkQm7hjfkZGwXyeNwQAsjyfvjzp6SRg/3R/
    Kgp246tTNCh0gBRn8shkBcg7BumnqJc3AueQCVWu2C2lcovpEx7ZCL2gVLsfx8u/
    zGbSaPsMC2K84u4TBZtB5GE/xu1p87rE50V9Xq9YYKSdsx9ViWvUTI2ky8mxOxjS
    2ythSq9lrR7OrB727+N8nUMoCIINIgP+KiVQB6448Cld2CrTzHv92cV2oCyqOFSb
    9lZbfyrcVculAnxsRXKzAHdr5ZcQZNVqXV/jisP954+hWoL46mUv5nrS4dynxppd
    bHA9n4zomVKpuFT3eG0jqazUunmP+j1vfCJIFEuJDxugUsJA943K0+musCmZ1PfX
    kg5UiKlvUzY=
    =q7Sv
    -----END PGP PUBLIC KEY BLOCK-----
    `;
 
    console.log((await openpgp.key.read(publicKeyArmored)));

    const { data: encrypted } = await openpgp.encrypt({
        message: openpgp.message.fromText('Hello, World!'),                 // input as Message object
        publicKeys: (await openpgp.key.read(publicKeyArmored)).keys, // for encryption
        privateKeys: []                                           // for signing (optional)
    });
    console.log(encrypted); // '-----BEGIN PGP MESSAGE ... END PGP MESSAGE-----'
    
})();