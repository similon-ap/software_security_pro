<!-- 
DISCLAIMER:

!!!!

GEBRUIK NIET DIT BESTAND OM HET LABO TE MAKEN MAAR GEBRUIK DE LINK OP DIGITAP!

!!!!
-->

<style>
.holder_default {
    width:500px; 
    height:150px; 
    border: 3px dashed #ccc;
}

.hover { 
    width:400px; 
    height:150px; 
    border: 3px dashed #0c0 !important; 
}

.hidden {
    visibility: hidden;
}

.visible {
    visibility: visible;
}
</style>

<script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

# Hashing en Encryptie

Zoals altijd kunnen de oefeningen binnengehaald worden door een git pull te doen.

```
git pull
```

of de git repository te clonen als je deze nog niet hebt.

```
git clone https://bitbucket.org/similon-ap/software_security_pro.git
```

### Wat ga je leren in dit labo?
- We herhalen de reeds geleerde concepten over hashing en encryptie. 
- We herhalen het gebruik van de crypto.js library. 

    Meer informatie op https://app.gitbook.com/@apwt/s/g-pro-software-security/hashing/crypto.js

- Kijk dan zeker ook nog eens de vorige labo's over hashing en encryptie na.

### Stappenplan

1. Open de terminal in ```labo_hashing_encryption/exercise1``` via visual studio code en installeer alle npm dependencies met

    ```
    npm install
    ```

2. In dit labo zijn we aangenomen om de bank 'UNSAFE BANK INC.' te helpen met hun security problemen. Jij bent uiteraard na de cursus Software Security de aangewezen persoon om hun daarbij te helpen.

3. Ze hebben ons een script aangeboden dat hun ```users``` databank afprint. Je kan het script laten lopen door
    ```
    node password_db_print.js
    ```

    uit te voeren in je terminal. 

4. Om in te loggen hebben ze ook een script aangeboden. Je kan het laten uitvoeren door 
    ```
    node login.js
    ```
    in de terminal uit te voeren. Log in met 1 van de users die je in de vorige stap hebt gezien.

5. Je ziet hier al een groot probleem. Als hackers dit script te pakken krijgen kunnen ze gewoon de passwoorden uitlezen. Daarom moeten we deze passwoorden ```hashen``` zodat deze niet meer leesbaar zijn voor hackers indien deze zouden gestolen worden.

6. Ga naar het bestand ```security.js``` en pas de ```hash``` functie aan zodat deze het ```PBKDF2``` algoritme gebruikt om het passwoord te hashen. Zorg ervoor dat er maar 1 iteration wordt gedaan en dit de salt gebruikt die daar boven aangemaakt wordt. We willen hier de hexadecimale string representatie van.

    ***Opgelet***: gebruik hiervoor de ```iterations``` en ```salt``` constante die boven de functie al klaarstaat.

7. Voer nu terug het 

    ```
    node password_db_print.js
    ```

    script uit. Nu zal je zien dat de paswoorden niet meer zomaar uitleesbaar zijn.

8. Na je wijziging klagen de klanten van de bank dat ze niet meer kunnen inloggen met het ```login.js``` script.

    Dit komt omdat het passwoord dat je ingeeft ook eerst moet gehashed worden met de ```hash``` functie vooraleer je die aan de ```login``` functie moet meegeven. Pas het ```login.js``` bestand aan zodat je terug kan inloggen. 

9. Je hebt al opgemerkt dat er in de output vaak:

    ```[DEBUG] hash(admin123) took 1 ms```

    stond. Dit is omdat we ook de snelheid van het hashing algoritme willen testen. Met 1 iteratie ging dit heel snel. Veel te snel! Dit betekent dat een hacker dit zelf ook heel snel kan berekenen. Hij moet dit natuurlijk wel voor alle combinaties testen. 

    De bank wil dat jij het aantal iteraties van het hashing algoritme verhoogt zodat dit gemiddeld gezien ongeveer 200ms zal duren om een hash te berekenen. 

    Test de snelheid van je hashing functie door

    ```
    node hashing_speed_test.js
    ``` 

    uit te voeren. Indien het niet rond 200ms ligt, pas je het aantal iterations in ```security.js``` aan en herhaal je de stap. 

10. Nu je al de login problemen hebt opgelost van de bank is het tijd om een ander probleem op te lossen. Ze hebben gezien wat je gedaan hebt met het hashing algoritme, en zouden graag hebben dat jij hetzelfde doet voor het beveiligen van hun intern email verkeer.

    Uiteraard laat jij hun weten dat dit niet met hashing wordt gedaan, want hashing algoritmes zijn niet omkeerbaar, en dus zou de ontvanger dit bericht nooit meer kunnen ontcijferen.

    Jij stelt voor: 'We gebruiken symmetrische encryptie!' 

11. Bij het uitvoeren van

    ```
    node communication.js
    ```

    wordt het snel duidelijk dat de encryptie nog altijd niet in orde is. 

    Pas de functies ```decrypt``` en ```encrypt``` aan in het ```security.js``` bestand zodat de text geencrypteerd wordt met het **DES** algoritme. Loop daarna het script terug opnieuw.

    **Opgelet:** Vergeet bij het decrypteren het niet terug om te zetten naar een UTF-8 string.

12. De bank merkt wel op dat ze wel niet graag met paswoorden werken om te communiceren met de andere banken. Zoiets geraakt uiteraard te vaak verloren. Jij als security expert weet direct wat je moet doen. Je stelt **asymmetrische encryptie** voor. 

    Zo zal het nodig zijn dat elke bank een publieke sleutel aanmaakt. Als je dan met die bank wil communiceren moet je simpelweg je bericht encrypteren met de publieke sleutel van die bank. Zo zijn paswoorden helemaal niet meer nodig. De ontvangende bank kan dan het bericht decrypteren aan de hand van zijn eigen private sleutel (die hij nooit weggeeft)

13. Je wil deze functie niet zelf maken. Dus je stelt een korte demo voor via de website

    https://smartninja-pgp.appspot.com/

14. Genereer zelf een PGP keypair voor jezelf. Kies RSA als algoritme en een keysize van 1024bit. Zorg ervoor dat de sleutel nooit vervalt.
    
    Kopieer je eigen public key in het bestand ```public_key.txt```

15. Probeer een bericht te encrypteren met de volgende publieke sleutel. Je mag hier zelf kiezen wat je stuurt. 

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mI0EXm6FEAEEAModXdF/WFe3gEgSHQWhJYgNNm4RFD82Ed02ebTA/V8rmRdpPj1G
WiK0Q2yeXU148Jk2o26OzjzUY5OtZSnsL/JmApvxQll9VJw9fmmnQ4QZiqZbJ6T8
xUfE0vCDH+JoEJAh/Qx+fpqdvCPHMKQ6e8sqwV0iVkQVyqlA68ro0bNXABEBAAG0
I0FuZGllIFNpbWlsb24gPGFuZGllLnNpbWlsb25AYXAuYmU+iLAEEwEKABoFAl5u
hRACGy8DCwkHAxUKCAIeAQIXgAIZAQAKCRABLFt/RKGQbM7XBACZEPj3BLq314Yo
Gq5ZvZy2XpjpWF7RlsjzxYuzZpiVeGBJcM1tU9pMcH+RhUgBkLEMqTYFhXYnDFXF
va2JKtwcHGub63R1Tsi0Hldfpfn7eaoJyzde/O96nGbyBtlM6ovqDDnp+Agzi+y6
PsvmaxIcxDGTUlFx/ki5sPwzCWVGXriNBF5uhRABBACudfMK0ROt/FGuVSTMsT2t
9vlPv8jRz03b3MzFQxJ+4rhlmu/vEYyZbuPW7c+kF3Wt3VjwAamM8k064TmjXeE2
CWjFRuzJDZwhcl4Bhj432C9q5NZa40vlan5nlU4zKz6aAiT4ScASOP4ytdzrp8F3
iyPX0glKqYsEq5mSlh+WXwARAQABiQFDBBgBCgAPBQJeboUQBQkPCZwAAhsuAKgJ
EAEsW39EoZBsnSAEGQEKAAYFAl5uhRAACgkQqjKNi2I2VEXzmAQAhN1VIOSNSSgf
SS29EqpsFzc9KqBtpmwkCyn0/T6Ug9ZHL+Zbgzb1t56gj8U4/kJkqyDUV5MIYnKx
BHSrKTmyh92BdSyyKcNHx5ew51njh+Z47ekRRA2y6/wy2aB2V7aT4apIEwifJy9G
yg8EL6oHwqZP7o3ozikXpUJWmroUL5AQeAP+Kk9FWpRLSuucEETrX85ySqbyLlL5
liL4NP1lxKwUxjoWK7JMfu7jbgqcpSTthvDXjih/He2TazjjaaAv8B2fQ2mpRUbH
d52M+f+GYpKGoAWFui+67tfmdhh+U8b1VaZ3qnt1InNIcPQp3bd1L8ovXBhqSTSe
GbuZR1/syzCCg224jQReboUQAQQA2eThBTTU1YXCOAXdHjD3WVG67JdRa8gexsmt
g6DvmhztmEmT/heWgagxXEL/LEkhKmhKC9hRgTJJxJXwVkBhFYkvDxHB4Gx6cf60
GRWQBDjnIbkdos8a3coyGWhhFElCZ/1C1Bo1oCke7UpRiYsho1Dc1mpcYNqDyjqJ
wFbJopsAEQEAAYkBQwQYAQoADwUCXm6FEAUJDwmcAAIbLgCoCRABLFt/RKGQbJ0g
BBkBCgAGBQJeboUQAAoJENaqHCN8sZrLJHwD/Roar4jUkM7fiSzjISirDbAIUGE3
/Gs+gWvgeZUXd//ZVy5qReR1oewp6/aCWfxxEr+JmyFbP7ngSrOkATJagx2Y5WgL
Ux5a/obUd0KAT9z11ZmLnFA1pPv0KpgK6qEu4fBnpw7dJtECi+F+U8cJMtOdA9vL
mgjqNHpNnJISLZ5npQEEAMkp/e0yT+mUmojSYWMQ3gCtocPYLLpzVsa1ppmH757a
qscD9GWpi4tlC6d4zrhDRvS9Pd65hmVvS1TrNiJPctxR0PcClT9TMhoN11v9SPP8
g7GAXEeb89Dp9YTIqWhixFAJMjcWjZssqgfWI56Jd0i7l6QAIfJCawIGoBh7QFf/
=Bhpk
-----END PGP PUBLIC KEY BLOCK-----
```

16. Slaag het resultaat op in ```encrypted.txt```

17. Maak een **zip** file (liefst geen rar) van alle javascript en txt bestanden en stuur deze door via digitap. De ```node_modules``` moeten hier niet toegevoegd worden. Ze vergroten alleen maar je **zip** file. 