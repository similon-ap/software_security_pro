const winston  = require('winston');
const {Loggly} = require('winston-loggly-bulk');


module.exports = function(labName) {
    const re = /\S+@\S+\.\S+/;
    let email = process.argv[2];
    if (!email || !re.test(email)) {
        console.log('Start the application using your email adress!');
        process.exit();
    }

    winston.add(new Loggly({
        token: 'ad822c65-6ee0-4be5-af4c-487e00703b35',
        subdomain: 'slimmii',
        tags: [labName],
        json: true
    }));
    winston.log('info', `${email} started the lab ${labName}`)

    return {
        log: (log) => {
            winston.log('info', `${email} - ${log}`);
        },
        getEmail: () => {
            return email;
        }
    };

};


