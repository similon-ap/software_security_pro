const hashing = require('./hashing');

describe('testing the hashing library', () => {
    test('test the md5 function', () => {
        expect(hashing.MD5('test')).toBe('098f6bcd4621d373cade4e832627b4f6');
    });

    test('test the md5 with salt function', () => {
        expect(hashing.MD5withSalt('test','b5f5a1ab62b63e38a73dbfa57cc18b46')).toBe('2fdb6ad0e6707afec2e20047d7f83eb6')
    });

    test('test the md5 with salt and 1 iterations is same as md5 with salt', () => {
        expect(hashing.MD5withSaltAndIterations('test','b5f5a1ab62b63e38a73dbfa57cc18b46',1)).not.toBeUndefined();
        expect(hashing.MD5withSaltAndIterations('test','b5f5a1ab62b63e38a73dbfa57cc18b46',1)).toBe(hashing.MD5withSalt('test','b5f5a1ab62b63e38a73dbfa57cc18b46'));
    });

    test('test the md5 with salt and 10000 iterations is different than 1 iteration', () => {
        expect(hashing.MD5withSaltAndIterations('test','b5f5a1ab62b63e38a73dbfa57cc18b46',10000)).not.toBe(hashing.MD5withSalt('test','b5f5a1ab62b63e38a73dbfa57cc18b46'));

    })

    test('test de PBKDF2 functie', () => {
        expect(hashing.PBKDF2('test', 'b5f5a1ab62b63e38a73dbfa57cc18b46', 4, 1000).toString()).toBe('b91e7ba1ade6a1614d6e56150d434e1b');
    })

})