# Hashing Labo

Zoals altijd kunnen de oefeningen binnengehaald worden door een git pull te doen.

```
git pull
```

of de git repository te clonen als je deze nog niet hebt.

```
git clone https://bitbucket.org/similon-ap/webontwikkeling.git
```

## Hashing voor checksum 
### Uitleg

Als je een bestand download van een publieke website wordt er vaak een checksum meegegeven. Met deze checksum kan je nakijken of de file die je gedownload hebt niet aangepast is door een kwaadwillige partij.

### Stappenplan

1. Op digitap heb ik 2 verschillende files geplaatst in de map "Hulpbestanden Labo". Download deze bestanden .
2. Open Visual Studio Code op de locatie waar je de files hebt gedownload en voer het volgende commando uit in je terminal op de gedownloade bestanden.

    ```bash
    // Windows
    CertUtil -hashfile filename MD5
    // Mac of linux
    md5 filename
    ```


    de verwachte md5 hash is

    ```
    MD5 (file) = df036ddf780f573b57445f23c25df7d3
    ```

3. Welke is nu het echte en welk is het aangepaste?

---

## Hashing voor passwoorden
### Uitleg

Paswoorden worden meestal opgeslagen als een hash. Om een paswoord te verifieren, wordt er een hash berekend van het paswoord dat je hebt ingegeven en dan wordt het vergeleken met het paswoord in de database. Het MD5 algoritme van vorige sessie is niet voldoende veilig voor paswoorden op te slagen. Dit ga je in dit deel ontdekken!

### Documentatie

Vooraleer je begint kijk eens naar
https://cryptojs.gitbook.io/docs/#hashing 

### Stappenplan
**Deel 1:**
1. Open labo_hashing/exercise1
2. Installeer alle dependencies 

    ```npm install```

3. Ga naar de module 'hashing.js'
4. Vul de MD5 functie aan zodat deze het passwoord hashed met het MD5 algoritme. Zorg ervoor dat deze teruggegeven wordt als hex string.

    *Kijk in de documentatie hoe je een md5 hash terugkrijgt en hoe je de hex string verkrijgt.*
5. Voer de testen uit en zie dat de test voor md5 slaagt.

    ```npm test```

6. Ga naar 'index.js' en roep deze functie op met als passwoord een woord van max 8 karakters. Doe dit voor 5 verschillende paswoorden.

    **PAS OP**: Gebruik geen echte passwoorden!
7. Start de applicatie

    ```node index.js```

8. Ga voor elk van deze MD5 hashes naar de volgende url (gewoon in je browser): 
    
    http://www.nitrxgen.net/md5db/[md5hash]
    
    (bv: http://www.nitrxgen.net/md5db/c3584ce54aca1de30caea1ceaae99ee0)

9. Hoeveel wachtwoorden zouden veilig geweest zijn als de website je wachtwoord hashed met MD5?

10. Vul nu de functie MD5withSalt in hashing.js in. Deze functie doet exact hetzelfde als de MD5 functie maar geeft de concatenatie (samenvoeging) van password en salt terug. 

11. Voer de testen uit en zie dat de test voor MD5withSalt slaagt.

    ```npm test```

12. Roep nu de functie in index.js en geef als 2de parameter een salt mee:

    ```javascript
    let salt = CryptoJS.lib.WordArray.random(16)
    ```

13. Probeer nu net zoals 8. de md5 hashes uit op http://www.nitrxgen.net/md5db

14. Merk op dat geen enkele md5 hash nog voorkomt in de search.

15. Vul nu de MD5withSaltAndIterations functie in hashing.js in. Deze functie gaat gewoon de MD5withSalt X keer uitvoeren. Neem telkens de output van de vorige iteratie als nieuwe input.

    Door dit te doen wordt de md5 hash functie zwaarder om uit te voeren dus ook veel moeilijker om een groot aantal keer na elkaar uit te voeren om het paswoord te raden.

16. Nu heb je zelf een hashing algoritme gemaakt dat een salt gebruikt en meerdere keren kan uitgevoerd worden. Dit ga je in de realiteit **NOOIT** doen. Je gebruikt hiervoor altijd best een veel gebruikte en gekende functie zoals **PBKDF2**.

17. Vul nu de functie PBKDF2 in die een paswoord aanvaard, een salt, een keySize en een parameter iterations aanvaard.

    Kijk naar de documentatie van PBKDF2 om te zien hoe je ze gebruikt.

**Deel 2:**

1. Je hebt een md5 hash gevonden van een pincode van 4 cijfers in de bank. 

    ```
    81dc9bdb52d04dc20036dbd8313ed055
    ```

    Bereken de md5 has van alle mogelijke pincodes om uit te vinden wat de originele pincode was.

    TIP: Een getal omzetten naar 4 cijfers doe je op de volgende manier:
    ```
    getal.toString().padStart(4,'0');
    ```

2. Je hebt een PBKDF2 hash gevonden.

    ```
    hash: 5f5e1952a84387042a7e396cf98bff6a
    salt: 15f5120a05728a4080aaa3f164befb9f
    ```

    Iemand in het systeem verklapt je dat ze een keySize van 4 gebruiken en altijd 1000 iterations gebruiken.

    Wat is de pincode?

---